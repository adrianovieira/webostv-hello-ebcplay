# Hello, EBC PLay

*Hello, EBC Play!* Simple sample webOS TV's app

## Emulator's screenshots

*Welcome page*

![](docs/images/hello-welcome.png)

*Hello, EBC Play!*

![](docs/images/hello-ebcplay-1.png)

![](docs/images/hello-ebcplay-2.png)

## SDK Installation

![SDK](docs/images/sdk-installation.png)  
*[SDK Installation](http://webostv.developer.lge.com/sdk/installation/)*

### Enact framework

> "***Enact*** *builds atop the excellent React library*" - Enact

![Enact](docs/images/enact-framework.png)  
*[Enact framework](https://enactjs.com/)*